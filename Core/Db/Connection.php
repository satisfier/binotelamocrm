<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.12.16
 * Time: 13:16
 */

namespace Core\Db;

class Connection
{
    private static $instance;

    private $db;

    public static function getInstance() {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return self::$instance;
    }

    public function query($sql, $params = []) {
        $result = null;
        
        try {
            $statement = $this->db->prepare($sql);
            $result = $statement->execute($params);
        } catch (\PDOException $e) {
            echo $e->getMessage() . PHP_EOL;
        }

        return $statement;
    }

    private function __construct() {
        $config = require_once(__CONFIG__ . "/db.php");
        $dsn = 'mysql:dbname=' . $config['dbname'] . ';host=' . $config['host'];

        try {
            $this->db = new \PDO($dsn, $config['user'], $config['password']);
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    private function __wakeup() {}

    private function __clone() {}
}