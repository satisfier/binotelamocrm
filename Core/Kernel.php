<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29.11.16
 * Time: 18:38
 */

namespace Core;

class Kernel
{
    const CONTROLLERS_NAMESPACE = '\\Controllers\\';

    public function execute($controller, $method) {
        $className = self::CONTROLLERS_NAMESPACE . ucfirst($controller) . 'Controller';
            
        if (class_exists($className)) {
            if (method_exists($className, $method)) {
                $class = new $className();
                
                $this->output($class->$method());
            } else {
                throw new \Exception("$controller has no '$method' method");
            }
        } else {
            throw new \Exception("Controller '$controller' doesn't exist");
        }
    }

    protected function output($data = []) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}