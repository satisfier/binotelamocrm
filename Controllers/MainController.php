<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.11.16
 * Time: 15:40
 */

namespace Controllers;

use Core\Db\Connection;
use Helpers\AmoCRM\AmoCRMHelper;
use Helpers\Binotel\BinotelHelper;

class MainController extends BasicController
{

    public function update() {
        $user = $this->amoCRM->getCurrentAccount();
        $calls = $this->getCalls();
        
        if (empty($calls)) {
            return [
                'response' => 'up to date'
            ];
        }

        foreach ($calls as $call) {
            $contact = $this->amoCRM->getContact(['query' => '38' . $call['externalNumber']]);

            if (null == $contact) {

                $lead = $this->amoCRM->addLead([
                    'name' => '38' . $call['externalNumber'],
                    'responsible_user_id' => (int) $user['response']['account']['id'],
                    'date_create' => (int) $call['startTime'],
                    'status_id' => self::$leadStatusId
                ]);

                $leadId = $lead['response']['leads']['add'][0]['id'];

                $addContact = [
                    'name' => '38' . $call['externalNumber'],
                    'responsible_user_id' => (int) $user['response']['account']['id'],
                    'date_create' => (int) $call['startTime'],
                    'linked_leads_id' => (int) $leadId,
                    'custom_fields' => [
                        [
                            'id' => self::$phoneFieldId,
                            'name' => 'Телефон',
                            'code' => 'PHONE',
                            'values' => [
                                [
                                    'value' => '38' . $call['externalNumber'],
                                    'enum' => self::$phoneFieldEnum
                                ],
                            ]
                        ]
                    ],
                ];

                $this->amoCRM->addContact($addContact);
            } elseif ($contact['response']['contacts'][0]['date_create'] !== (int) $call['startTime']) {

                $lead = $this->amoCRM->addLead([
                    'name' => '38' . $call['externalNumber'],
                    'responsible_user_id' => (int) $user['response']['account']['id'],
                    'date_create' => (int) $call['startTime'],
                    'status_id' => self::$leadStatusId
                ]);

                $leadId = $lead['response']['leads']['add'][0]['id'];
                $updatedLeads = $contact['response']['contacts'][0]['linked_leads_id'];
                $updatedLeads[] = $leadId;
                $updatedLeads = array_map('intval', $updatedLeads);

                sleep(2);

                $data = [
                    'id' => $contact['response']['contacts'][0]['id'],
                    'linked_leads_id' => $updatedLeads,
                    'last_modified' => time()
                ];

                $this->amoCRM->updateContact($data);
            }

            sleep(2);
        }

        return true;
    }

    public function getCalls() {
        $options = [
            'timestamp' => strtotime($this->config['interval'])
        ];
        return $this->binotel->getLastCalls($options)['callDetails'];
    }

    public function checkAuth() {
        try {
            $this->amoCRM->getCurrentAccount();
        } catch (\Exception $e) {
            /* Only if Unauthorized */
            if ($e->getCode() === 401) {
                $this->amoCRM->auth();
            }
        }
    }
    
    public function getLeadLinks() {
        return $this->amoCRM->getLeadLinks();
    }

    /* Tests */
    public function getLeads() {
        return $this->amoCRM->getLeads(['query' => '380500108046']);
    }

    public function getContacts() {
        return $this->amoCRM->getContact(['query' => '380500108046']);
    }
}