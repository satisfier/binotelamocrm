<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.12.16
 * Time: 15:08
 */

namespace Controllers;

use Helpers\AmoCRM\AmoCRMHelper;
use Helpers\Binotel\BinotelHelper;

class BasicController
{
    protected $amoCRM;
    protected $binotel;

    protected $config;

    public static $phoneFieldId = 1314728;
    public static $phoneFieldEnum = 3157506;

    public static $leadStatusId = 12703506;

    public function __construct() {
        $this->amoCRM = new AmoCRMHelper();
        $this->binotel = new BinotelHelper();

        $this->config = require_once(__CONFIG__ . "/api.php");
    }
}