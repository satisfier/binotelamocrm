<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.12.16
 * Time: 15:06
 */

namespace Controllers;

/**
 * Class ExportController
 * @package Controllers
 */
class ExportController extends BasicController
{
    public function addClient() {
        return $this->binotel->addClient($this->parseData());
    }

    /**
     * @return array
     */
    public function parseData() {
        $data = json_decode(file_get_contents('php://input'), true);
        $contact = $data['contacts']['update'];

        $name = $contact['name'];
        $number = $contact['custom_fields'][0]['values'][0]['value'];

        return [
            'name' => $name,
            'phone' => $number,
            'description' => '',
            'email' => ''
        ];
    }
}