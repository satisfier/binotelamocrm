<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.11.16
 * Time: 15:37
 */

spl_autoload_register(function ($className) {
    $class = str_replace('\\', '/', $className) . '.php';
    require_once($class);
});