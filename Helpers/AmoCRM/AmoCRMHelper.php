<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.11.16
 * Time: 15:42
 */

namespace Helpers\AmoCRM;

class AmoCRMHelper
{
    const SUB_DOMAIN = 'new580b6aa0264f9';
    const API_KEY = '7d0dc271f221669fe08b488359528c55';
    const LOGIN = 'lid@ukrsecurity.kiev.ua';

    const BASE_URL = 'https://' . self::SUB_DOMAIN . '.amocrm.ru/private/api/';
    const API_URL = self::BASE_URL . 'v2/json/';
    const AUTH_URL = self::BASE_URL . 'auth.php?type=json';

    protected $disableSSL = true;

    protected $errors = [];

    public function __construct() {
        $this->init();
    }

    public function getContact($options = []) {
        $list = $this->sendGetRequest('contacts/list', $options);

        return $list;
    }

    public function addContact($add = []) {
        $result = $this->sendPostRequest('contacts/set', [
            'request' => [
                'contacts' => [
                    'add' => [
                        $add
                    ]
                ]
            ]
        ]);

        return $result;
    }

    public function addLead($add = []) {
        $result = $this->sendPostRequest('leads/set', [
            'request' => [
                'leads' => [
                    'add' => [
                        $add
                    ]
                ]
            ]
        ]);

        return $result;
    }

    public function updateContact($options = []) {
        return $this->sendPostRequest('contacts/set', [
            'request' => [
                'contacts' => [
                    'update' => [
                        $options
                    ]
                ]
            ]
        ]);
    }

    public function getLeads($options = []) {
        return $this->sendGetRequest('leads/list', $options);
    }

    public function getLeadLinks() {
        return $this->sendGetRequest('contacts/links');
    }

    public function getCurrentAccount() {
        $response = $this->sendGetRequest('accounts/current');

        return $response;
    }

    public function sendGetRequest($url, $options = []) {
        $response = $this->request(self::API_URL . $url, $options);

        return $response;
    }

    public function sendPostRequest($url, $options = []) {
        $response = $this->request(self::API_URL . $url, $options, true);

        return $response;
    }

    public function auth() {
        $credentials = [
            'USER_LOGIN' => self::LOGIN,
            'USER_HASH' => self::API_KEY
        ];

        $data = $this->request(self::AUTH_URL, $credentials, true);

        if (isset($data['response']['auth'])) {
            return true;
        }
        return false;
    }

    protected function init() {
        $this->errors = [
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            429 => 'Too many queries',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        ];
    }

    protected function request($url, $options = [], $post = false) {
        $ch = curl_init();

        if (!$post && isset($options)) {
            $url = $url . '?' . http_build_query($options);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . '/auth/cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . '/auth/cookie.txt');

        if ($this->disableSSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        if ($post) {
            $data = json_encode($options);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Length: ' . mb_strlen($data),
                'Content-Type: application/json'
            ]);
        }

        $result = curl_exec($ch);
        $code = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($code !== 200 && $code !== 204) {
            throw new \Exception(isset($this->errors[$code]) ? $this->errors[$code] : 'Unknown error', $code);
        }

        return json_decode($result, true);
    }
}