<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.11.16
 * Time: 15:42
 */

namespace Helpers\Binotel;

use Helpers\Binotel\BinotelApi;

class BinotelHelper
{
    const KEY = 'ffe495-961602a';
    const SECRET = 'c1a08b-1701a1-ef3b50-3351d3-79a95e22';

    public static $assignedToEmployeeNumber = 19875;
    protected $client;

    public function __construct() {
        $this->client = new BinotelApi(self::KEY, self::SECRET);
    }

    public function getHelper() {
        return $this->client;
    }

    public function getAllCalls($options = []) {
        $result = $this->client->sendRequest('stats/list-of-calls-per-day', $options);

        return $result;
    }

    public function getLastCalls($options = []) {
        $result = $this->client->sendRequest('stats/all-incoming-calls-since', $options);
        
        return $result;
    }

    public function addClient($options = []) {
        $result = $this->client->sendRequest('customers/search', [
            'subject' => $options['phone']
        ]);

        if ($result['status'] === 'success') {
            $status = null;
            if (empty($result['customerData'])) {
                $status = $this->client->sendRequest('customers/create', [
                    'name' => $options['name'],
                    'numbers' => [
                        $options['phone']
                    ],
                    'description' => $options['description'],
                    'email' => $options['email'],
                    'assignedToEmployeeNumber' => self::$assignedToEmployeeNumber
                ]);
            } elseif (!empty($result['customerData'])) {
                $status = $this->client->sendRequest('customers/update', [
                    'id' => (int) key($result['customerData']),
                    'name' => $options['name'],
                    'numbers' => [
                        $options['phone']
                    ],
                    'description' => $options['description'],
                    'email' => $options['email'],
                    'assignedToEmployeeNumber' => self::$assignedToEmployeeNumber
                ]);
            }

            return $status;
        }

        return [
            'success' => false
        ];
    }
}