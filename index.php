<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.11.16
 * Time: 15:37
 */

require_once "bootstrap.php";

if (php_sapi_name() == "cli") {
    $kernel = new \Core\Kernel();
    $kernel->execute('main', 'checkAuth');
    $kernel->execute('main', 'update');
} else {
    throw new \Exception("Only for cli");
}